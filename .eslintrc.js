module.exports = {
  'env': {
    'browser': true,
    'es2021': true
  },
  'extends': [
    'google'
  ],
  'parserOptions': {
    'ecmaVersion': 12,
    'sourceType': 'module'
  },
  'rules': {
    'require-jsdoc': ['error', {
      'require': {
        'FunctionDeclaration': false,
        'MethodDefinition': false,
        'ClassDeclaration': false,
        'ArrowFunctionExpression': false,
        'FunctionExpression': false
      }
    }],
    'object-curly-spacing': ['error', 'always'],
    'max-len': ['error', { 'code': 110 }],
    'arrow-parens': ['error', 'as-needed'],
    'comma-dangle': ['error', 'never'],
    'no-compare-neg-zero': 'error',
    'no-console': 'error',
    'no-debugger': 'error',
    'no-dupe-args': 'error',
    'no-dupe-keys': 'error'
  }
};
