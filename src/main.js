import { SectionCreator } from './SectionCreator';
import './styles/style.css';

document.addEventListener('DOMContentLoaded', () => {
  new SectionCreator().create('standard').render();
});
