export const JOIN_SECTION_DESCRIPTION =
  'Sed do eiusmod tempor incididunt \nut labore et dolore magna aliqua.';
export const JOIN_PROGRAM_HEADER = 'Join Our Program';
export const JOIN_PROGRAM_HEADER_ADVANCED = 'Join Our Advanced Program';
export const JOIN_PROGRAM_EMAIL_PLACEHOLDER = 'Email';
export const JOIN_PROGRAM_BUTTON_SUBSCRIBE = 'Subscribe';
export const JOIN_PROGRAM_BUTTON_UNSUBSCRIBE = 'Unsubscribe';
export const JOIN_PROGRAM_BUTTON_SUBSCRIBE_ADVANCED = 'Subscribe to Advanced Program';
