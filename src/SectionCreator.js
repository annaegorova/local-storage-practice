import { addJoinUsSection, removeJoinUsSection } from './join-us-section';
import {
  JOIN_PROGRAM_HEADER,
  JOIN_PROGRAM_HEADER_ADVANCED,
  JOIN_PROGRAM_BUTTON_SUBSCRIBE,
  JOIN_PROGRAM_BUTTON_SUBSCRIBE_ADVANCED
} from './constants/text-constants';

export class SectionCreator {
  create(type) {
    switch (type) {
      case 'advanced':
        return new Section(JOIN_PROGRAM_HEADER_ADVANCED, JOIN_PROGRAM_BUTTON_SUBSCRIBE_ADVANCED);
      case 'standard':
      default:
        return new Section(JOIN_PROGRAM_HEADER, JOIN_PROGRAM_BUTTON_SUBSCRIBE);
    }
  }
}

class Section {
  constructor(headerText, buttonText) {
    this.headerText = headerText;
    this.buttonText = buttonText;
  }

  render() {
    addJoinUsSection(this.headerText, this.buttonText);
  }

  remove() {
    removeJoinUsSection();
  }
}
